from envparse import Env

env = Env()

REDIS_URL = env("REDIS_URL", default='redis.pythonweekend.skypicker.com')

DATABASE_URL = env(
    "DATABASE_URL",
    default="sql.pythonweekend.skypicker.com"
    # default="postgresql://pythonweekend:QA1rWe0HcGo4sNrf@sql.pythonweekend.skypicker.com/pythonweekend"
)

DATABASE_TYPE = env("DATABASE_TYPE", default="postgresql")

DATABASE_DB = env("DATABASE_DB", default="pythonweekend")

DATABASE_USER = env("DATABASE_USER", default="pythonweekend")

DATABASE_PASS = env("DATABASE_PASS", default="testing")