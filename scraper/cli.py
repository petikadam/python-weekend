import argparse
from .core.scraper import RegiojetScraper
from .core import get_routes
from .models import ScrapeRequest
import pprint
from .core import RedisCache

def parse_args():
    parser = argparse.ArgumentParser(description='xy')
    parser.add_argument('source', type=str, help='source')
    parser.add_argument('destination', type=str, help='destination')
    parser.add_argument('depdate', type=str, help='departure date')
    
    return parser.parse_args()


def main():
    args = parse_args()
    request = ScrapeRequest(src=args.source, dest=args.destination, depdate=args.depdate)

    redis_config = {
        'host': 'redis.pythonweekend.skypicker.com'
    }
    cache = RedisCache(key_prefix='petik:journey', config=redis_config)
    
    results = get_routes(request, scrapers=[RegiojetScraper()], cache=cache)
    pprint.pprint(results)


if __name__ == '__main__':
    main()