from sqlalchemy.sql.expression import true
from pydantic.tools import parse_obj_as
from sqlalchemy.orm.session import Session
from models import ScrapeResponse, Journey
from slugify import slugify
from models import ScrapeRequest
from typing import List
import json
import pprint

from db import create_journey

from .cache import Cache
from .scraper import Scraper


__all__ = ['get_routes']

 
def get_routes(
    request: ScrapeRequest,
    scrapers: List[Scraper],
    cache: Cache,
    db: Session,
) -> List[ScrapeResponse]:
    key = '_'.join([slugify(' '.join([request.src, request.dest]), separator='_'), request.depdate])
    cache_results = cache.get(key)
    routes: List[ScrapeResponse] = []
    if cache_results is None:
        print('not in cache, sraping...')
        for scraper in scrapers:
            routes += scraper.find_routes(request)
        # pprint.pprint(routes)
        cache.set(key, json.dumps([route.json() for route in routes]), ex=60)
    else:
        routes = parse_obj_as(List[ScrapeResponse], json.loads(cache_results))

    for route in routes:
        create_journey(db, journey=Journey(**route.dict()), merge=true)
    
    return sorted(routes, key=lambda x: x.departure_datetime)