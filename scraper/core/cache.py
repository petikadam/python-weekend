from abc import ABC, abstractmethod
from redis import Redis


__all__ = ['Cache', 'RedisCache']


class Cache(ABC):
    @abstractmethod
    def get(self, key):
        """Get the item by a key"""
    
    @abstractmethod
    def set(self, key, value, ex=None) -> bool:
        """Set the value for a given key"""
    
    @abstractmethod
    def clean_up(self) -> None:
        """Used for clean up"""


class RedisCache(Cache):
    def __init__(self, config, key_prefix = '') -> None:
        super().__init__()
        self.key_prefix = key_prefix
        self.redis_db = Redis(**config)
     
    def get(self, key):
        key = self._construct_key(key)
        return self.redis_db.get(key)

    def set(self, key, value, ex=None) -> bool:
        key = self._construct_key(key)
        return self.redis_db.set(key, value, ex=ex)

    def clean_up(self) -> None:
        self.redis_db.close()

    def _construct_key(self, key):
        return ':'.join([self.key_prefix, key])
