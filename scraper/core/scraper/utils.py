import json


def create_trip_dict(dep_time, arrival_time, source, destination, type_, carrier, price, source_id, destination_id, passangers):
    return {
        'departure_datetime': dep_time,
        'arrival_datetime': arrival_time,
        'source': source,
        'destination': destination,
        'type': type_,
        'carrier': carrier,
        'price': price,
        'source_id': source_id,
        'destination_id': destination_id,
        'passangers': passangers
    }

def do_city_code():
    with open('locations.json', 'r', encoding='utf-8') as f:
        locations = json.load(f)

    city_code = {}
    for country in locations:
        city_code.update({city['name']:city['id'] for city in country['cities']})

    with open('city_code.json', 'w', encoding='utf-8') as f:
        json.dump(city_code, f)