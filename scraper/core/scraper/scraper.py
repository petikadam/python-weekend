from abc import ABC, abstractmethod
from models.scrape_response import ScrapeResponse
from typing import Any, List
from models import ScrapeRequest


__all__ = ['Scraper']


class Scraper(ABC):
    def find_routes(self, request: ScrapeRequest):
        raw_data = self._get_raw_data(request)
        self._validate_raw_data(raw_data, request)
        parsed_routes = self._parse_raw_data(raw_data, request) 
        return [r for r in parsed_routes if self._route_condition(r, request)]

    def _validate_raw_data(self, raw_data: Any, request: ScrapeRequest):
        return
    
    @abstractmethod
    def _get_raw_data(self, request: ScrapeRequest) -> Any:
        pass
    
    @abstractmethod
    def _parse_raw_data(self, raw_data: Any, request: ScrapeRequest) -> List[ScrapeResponse]:
        pass  

    @abstractmethod
    def _route_condition(self, route: ScrapeResponse, request: ScrapeRequest) -> bool:
        pass  