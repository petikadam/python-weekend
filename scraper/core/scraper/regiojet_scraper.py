import json
from typing import List
import httpx
from datetime import datetime
from models import ScrapeRequest, ScrapeResponse

from .utils import create_trip_dict
from .scraper import Scraper


__all__ = ['RegiojetScraper']


class RegiojetScraper(Scraper):
    _url = 'https://brn-ybus-pubapi.sa.cz/restapi/routes/search/simple'

    def __init__(self) -> None:
        super().__init__()
        with open('/home/developer/project/city_code.json', 'r', encoding='utf-8') as f:
            self.city_code = json.load(f)
    
    def _get_raw_data(self, request: ScrapeRequest):
        params = {
            'departureDate': request.depdate,
            'fromLocationId': self.city_code[request.src],
            'toLocationId': self.city_code[request.dest],
            'fromLocationType': 'CITY',
            'toLocationType': 'CITY',
            'locale': 'cs',
            'tariffs': 'REGULAR'
        }
        headers = {'X-Currency': 'EUR'}
        response = httpx.get(self._url, headers=headers, params=params)
        return response.json()

    def _parse_raw_data(self, raw_data, request: ScrapeRequest) -> List[ScrapeResponse]:
        routes = raw_data['routes']
        return [self._route_to_response(route, request) for route in routes]

    def _route_to_response(self, raw_route, request: ScrapeRequest) -> ScrapeResponse:
        timeformat = "%Y-%m-%dT%H:%M:%S"
        return ScrapeResponse(
            departure_datetime=datetime.strptime(raw_route['departureTime'][:-10], timeformat),
            arrival_datetime=datetime.strptime(raw_route['arrivalTime'][:-10], timeformat),
            source=request.src,
            destination=request.dest,
            vehicle_type=''.join(raw_route['vehicleTypes']).lower(),
            carrier='REGIOJET',
            price=raw_route['priceFrom'],
            source_id=self.city_code[request.src],
            destination_id=self.city_code[request.dest],
            free_seats=raw_route['freeSeatsCount'],
            currency='EUR'
        )

    def _route_condition(self, route: ScrapeResponse, request: ScrapeRequest) -> bool:
        if route.free_seats >= request.passangers:
            return True
        else:
            return False