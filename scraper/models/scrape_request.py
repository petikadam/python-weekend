from dataclasses import dataclass
from datetime import date, datetime


__all__ = ['ScrapeRequest']


@dataclass
class ScrapeRequest:
    src: str
    dest: str
    depdate: str
    passangers: int = 1

    def get_datetime(self):
        return datetime.strptime(self.depdate, "%Y-%m-%d")