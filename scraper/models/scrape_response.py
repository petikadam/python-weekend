from pydantic import BaseModel
from datetime import datetime


__all__ = ['ScrapeResponse']


class ScrapeResponse(BaseModel):
    departure_datetime: datetime
    arrival_datetime: datetime
    source: str
    destination: str
    vehicle_type: str
    carrier: str
    price: float
    currency: str
    source_id: str
    destination_id: str
    free_seats: int

    class Config:
        json_encoders = {
            datetime: lambda v: v.strftime("%Y-%m-%dT%H:%M:%S")
        }
        orm_mode = True