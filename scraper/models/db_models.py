from sqlalchemy import Sequence, Column, Integer, String, TEXT, FLOAT
from sqlalchemy.dialects.postgresql import ENUM, TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base


__all__ = ['Journey', 'Base']


Base = declarative_base()


class Journey(Base):
    __tablename__ = 'journeys_op_v2'
    # id = Column(Integer, Sequence("journeys_v2_id"), primary_key=True)
    source = Column(TEXT, nullable=False, primary_key=True)
    destination = Column(TEXT, nullable=False, primary_key=True)
    departure_datetime = Column(TIMESTAMP, primary_key=True)
    arrival_datetime = Column(TIMESTAMP, primary_key=True)
    carrier = Column(TEXT, index=True, primary_key=True)
    vehicle_type = Column(ENUM("airplane", "bus", "train", name="vehicle_type_enum"))
    price = Column(FLOAT)
    currency = Column(String(3))
    source_id = Column(TEXT)
    destination_id = Column(TEXT)
    free_seats = Column(Integer)