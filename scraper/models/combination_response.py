from datetime import datetime
from pydantic.main import BaseModel
from .scrape_response import ScrapeResponse


__all__ = ['CombinationResponse']


class CombinationResponse(BaseModel):
    source: str
    stopover: str
    destination: str
    departure_datetime: datetime
    arrival_datetime: datetime
    price: float

    class Config:
        json_encoders = {
            datetime: lambda v: v.strftime("%Y-%m-%dT%H:%M:%S")
        }
        orm_mode = True