from typing import List
from sqlalchemy.orm import Session
from sqlalchemy.orm import aliased
from datetime import datetime

from models import CombinationResponse, ScrapeRequest, Journey


__all__ = [
    'get_journeys_by_destination',
    'create_journey',
    'get_combinations',
]


def get_journeys_by_destination(db: Session, destination: str):
    return db.query(Journey).filter(Journey.destination == destination).all()


def create_journey(db: Session, journey: Journey, merge: bool = False):
    if merge:
        db.merge(journey)
    else:
        db.add(journey)
    db.commit()

# def map_scrape_response_to_journey(scrape_response: ScrapeResponse):
#     return Journey(**scrape_response)

def get_combinations(db: Session, request: ScrapeRequest):
    combinations: List[CombinationResponse] = [] 
    Journey2: Journey = aliased(Journey)
    
    for segment_1, segment_2 in (
        db
            .query(Journey, Journey2)
            .join(Journey2, Journey.destination == Journey2.source)
            .filter(Journey.arrival_datetime < Journey2.departure_datetime)
            .filter(Journey.departure_datetime >= request.get_datetime())
            .filter(Journey.source == request.src)
            .filter(Journey2.destination == request.dest)
            .all()
    ):  
        combination = CombinationResponse(
            source=segment_1.source,
            stopover=segment_1.destination,
            destination=segment_2.destination,
            departure_datetime=segment_1.departure_datetime,
            arrival_datetime=segment_2.arrival_datetime,
            price=segment_1.price + segment_2.price,
        )
        combinations.append(combination)
    return combinations