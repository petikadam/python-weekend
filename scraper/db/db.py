from env_variables import DATABASE_DB, DATABASE_PASS, DATABASE_TYPE, DATABASE_URL, DATABASE_USER
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base

__all__ = ['SessionMaker']


_url = f'{DATABASE_TYPE}://{DATABASE_USER}:{DATABASE_PASS}@{DATABASE_URL}/{DATABASE_DB}'

engine = create_engine(_url, echo=True)

Base.metadata.create_all(bind=engine)

# # show debug information
SessionMaker = sessionmaker(engine)


