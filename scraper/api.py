from models.combination_response import CombinationResponse
from models.scrape_response import ScrapeResponse
from env_variables import REDIS_URL
from models import ScrapeRequest
from typing import List, Optional
from db import SessionMaker
from fastapi.param_functions import Depends
from core.scraper import RegiojetScraper, Scraper
from core import get_routes
from core import Cache, RedisCache
from redis import Redis
import uvicorn
from fastapi import FastAPI
from db import get_combinations

app = FastAPI()


def get_cache() -> Cache:
    redis_config = {
        'host': REDIS_URL
    }
    cache = RedisCache(key_prefix='petik:journey', config=redis_config)
    
    try:
        yield cache
    finally:
        cache.clean_up()

def get_scrapers() -> List[Scraper]:
    scrapers = [
        RegiojetScraper(),
    ]

    try:
        yield scrapers
    finally:
        pass

def get_db():
    db = SessionMaker()
    try:
        yield db
    finally:
        db.close()

@app.get('/search', response_model=List[ScrapeResponse])
def search_routes(
    source: str,
    destination: str,
    dateFrom: str,
    passangers: Optional[int] = 1,
    cache=Depends(get_cache),
    scrapers=Depends(get_scrapers),
    db=Depends(get_db),
):
    scrape_request = ScrapeRequest(source, destination, dateFrom, passangers=passangers)
    scrape_response = get_routes(scrape_request, scrapers, cache, db)
    return scrape_response

@app.get('/combinations', response_model=List[CombinationResponse])
def search_routes(
    source: str,
    destination: str,
    dateFrom: str,
    db=Depends(get_db),
):
    scrape_request = ScrapeRequest(source, destination, dateFrom)
    scrape_response = get_combinations(db, scrape_request)
    return scrape_response

if __name__ == '__main__':
    uvicorn.run(app, port=8002, host="0.0.0.0")